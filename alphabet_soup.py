import numpy as np

def parse_input(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()
    # First line contains the size of the grid
    size = tuple(map(int, lines[0].split('x')))
    grid_lines = lines[1:size[0]+1]
    word_lines = lines[size[0]+1:]

    # Creating the grid
    grid = [line.split() for line in grid_lines]
    words = [line.strip() for line in word_lines]

    return grid, words

def find_word_in_grid(grid, word):
    # Converting the grid to a numpy array for convenience
    grid = np.array(grid)
    rows, cols = grid.shape
    len_word = len(word)
   
    # Defining all possible search directions
    directions = [
        (1, 0),  # Horizontal left-to-right
        (0, 1),  # Vertical top-to-bottom
        (1, 1),  # Diagonal left-to-right
        (-1, 0), # Horizontal right-to-left
        (0, -1), # Vertical bottom-to-top
        (-1, -1),# Diagonal right-to-left
        (-1, 1), # Diagonal left-to-right upwards
        (1, -1)  # Diagonal right-to-left downwards
    ]

    # Searching for the word in all directions
    for row in range(rows):
        for col in range(cols):
            for dr, dc in directions:
                end_row, end_col = row + (len_word-1)*dr, col + (len_word-1)*dc
                if 0 <= end_row < rows and 0 <= end_col < cols:
                    word_formed = ''.join(grid[row + i*dr, col + i*dc] for i in range(len_word))
                    if word == word_formed:
                        return (row, col), (end_row, end_col)
    return None, None

def find_all_words(grid, words):
    results = []
    for word in words:
        # Removing spaces from the word before searching
        sanitized_word = word.replace(' ', '')
        start, end = find_word_in_grid(grid, sanitized_word)
        if start:
            results.append(f"{word} {start[0]}:{start[1]} {end[0]}:{end[1]}")
        else:
            results.append(f"{word} not found")
    return results

# Parsing the input content for the grid and words
input_file = 'word_search.txt'
grid, words = parse_input(input_file)

# Printing results
results = find_all_words(grid, words)
for result in results:
    print(result)
